import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder,Validators} from '@angular/forms';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {

  title : string;
  message : string[];
  

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {

    this.title = 'Hello-app';
    this.message = [
      '新しいコンポーネント',
      '複数のメッセージを表示できます',
      '属性は配列を設定します'
    ];
  
  }
}
